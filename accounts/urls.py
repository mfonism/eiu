from django.urls import path

from .views import SignUpDoneView, SignUpView

app_name = 'accounts'
urlpatterns = [
    path('signup/', SignUpView.as_view(), name='signup'),
    path('signup_done/', SignUpDoneView.as_view(), name='signup_done'),
]
