from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm as BaseUserCreationForm
from django.core.exceptions import ValidationError
from django.utils import timezone
from django.utils.translation import gettext, gettext_lazy as _

from . import services

UserModel = get_user_model()


class UserCreationForm(BaseUserCreationForm):
    is_tnc_agreed_to = forms.BooleanField(label=_('Terms and conditions agreement'), required=False)

    class Meta:
        model = UserModel
        fields = ['email', 'is_tnc_agreed_to']

    def clean_is_tnc_agreed_to(self):
        is_agreed_to = self.cleaned_data['is_tnc_agreed_to']
        if not is_agreed_to:
            raise ValidationError(_('Agreement to terms and conditions is required.'), code='invalid')
        return is_agreed_to

    def save(self, commit=True):
        user = super().save(commit=False)
        user.created_at = timezone.now()
        user.agreed_to_tnc_at = user.created_at

        if commit:
            user.save()

        services.send_signup_notification_email(user)
        return user
