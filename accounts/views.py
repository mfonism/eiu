from django.urls import reverse_lazy
from django.views.generic import CreateView, TemplateView

from .forms import UserCreationForm


class SignUpView(CreateView):
    form_class = UserCreationForm
    template_name = 'accounts/signup.html'
    success_url = reverse_lazy('accounts:signup_done')


class SignUpDoneView(TemplateView):
    template_name = 'accounts/signup_done.html'
