from django.core.mail import EmailMessage


def get_signup_notification_subject():
    pass


def get_signup_notification_body(user):
    pass


def send_signup_notification_email(user):
    message = EmailMessage(
        subject=get_signup_notification_subject(), body=get_signup_notification_body(user), to=[user.email]
    )
    message.send()
