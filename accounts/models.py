from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db import models
from django.utils.translation import gettext, gettext_lazy as _


class User(AbstractBaseUser, PermissionsMixin):
    username = None
    email = models.EmailField(verbose_name=_('email address'), max_length=255, unique=True)
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)
    created_at = models.DateTimeField()
    agreed_to_tnc_at = models.DateTimeField()

    USERNAME_FIELD = 'email'
