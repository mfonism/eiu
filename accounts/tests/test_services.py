from unittest import mock

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.utils import timezone

from accounts import services


UserModel = get_user_model()


class TestServices(TestCase):
    @classmethod
    def setUpTestData(cls):
        super().setUpTestData()

        cls.alice = UserModel(
            email='alice@etimfon.com',
            password='hashedpassword',
            created_at=timezone.now(),
            agreed_to_tnc_at=timezone.now(),
        )
        cls.alice.save()

    @mock.patch('accounts.services.EmailMessage')
    @mock.patch('accounts.services.get_signup_notification_subject')
    @mock.patch('accounts.services.get_signup_notification_body')
    def test_send_signup_notification_email(self, get_body_mock, get_subject_mock, mock_email_message):
        subject = get_subject_mock.return_value = 'a mock subject'
        body = get_body_mock.return_value = 'a mock body'

        services.send_signup_notification_email(self.alice)

        get_subject_mock.assert_called_once()
        get_body_mock.assert_called_once_with(self.alice)
        mock_email_message.assert_called_once_with(subject=subject, body=body, to=[self.alice.email])
        message = mock_email_message.return_value
        message.send.assert_called_once()
