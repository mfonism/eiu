from unittest import mock

from django.contrib.auth import get_user_model
from django.test import TestCase
from django.urls import reverse
from django.utils.translation import gettext, gettext_lazy as _


UserModel = get_user_model()


class TestSignUpView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.url = reverse('accounts:signup')
        cls.email = 'alice@etimfon.com'
        cls.password = 'iamA11ys!'
        cls.payload = {
            'email': cls.email,
            'password1': cls.password,
            'password2': cls.password,
            'is_tnc_agreed_to': True,
        }

    def get_form_errors(self, resp):
        return resp.context['form'].errors

    def test_get_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'accounts/signup.html')
        self.assertNotContains(resp, 'username')
        self.assertContains(resp, 'email')
        self.assertContains(resp, 'password1')
        self.assertContains(resp, 'password2')
        self.assertContains(resp, 'is_tnc_agreed_to')
        self.assertContains(resp, 'Signup')

    def test_signup(self):
        old_user_count = UserModel.objects.count()
        resp = self.client.post(self.url, self.payload)

        self.assertRedirects(resp, reverse('accounts:signup_done'))
        self.assertEqual(UserModel.objects.count(), old_user_count + 1)
        created_user = UserModel.objects.latest('created_at')
        self.assertIsNone(created_user.username)
        self.assertEqual(created_user.email, self.email)
        self.assertTrue(created_user.check_password(self.password))
        self.assertIsNotNone(created_user.created_at)
        self.assertIsNotNone(created_user.agreed_to_tnc_at)
        self.assertFalse(created_user.is_active)

    @mock.patch('accounts.services.send_signup_notification_email')
    def test_email_sent_on_signup(self, send_email_mock):

        resp = self.client.post(self.url, self.payload)

        self.assertRedirects(resp, reverse('accounts:signup_done'))
        created_user = UserModel.objects.latest('created_at')

        send_email_mock.assert_called_once_with(created_user)

    def test_signup_fails_if_tnc_is_not_agreed_to(self):
        old_user_count = UserModel.objects.count()
        payload = {**self.payload, 'is_tnc_agreed_to': False}
        resp = self.client.post(self.url, payload)

        self.assertEqual(resp.status_code, 200)
        self.assertIn(
            _('Agreement to terms and conditions is required.'), self.get_form_errors(resp)['is_tnc_agreed_to']
        )
        self.assertEqual(UserModel.objects.count(), old_user_count)


class TestSignUpDoneView(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.url = reverse('accounts:signup_done')

    def test_get_page(self):
        resp = self.client.get(self.url)

        self.assertEqual(resp.status_code, 200)
        self.assertTemplateUsed(resp, 'accounts/signup_done.html')
